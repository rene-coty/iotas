function checkbox_clicked(el) {
    window.webkit.messageHandlers.toPython.postMessage(
        {type: 'checkbox', id: el.id, checked: el.checked});
}
function task_div_clicked(event, id) {
    if (event.target.nodeName === 'A')
        return;
    el = document.getElementById(id);
    el.checked = !el.checked;
    window.webkit.messageHandlers.toPython.postMessage(
        {type: 'checkbox', id: el.id, checked: el.checked});
}
function add_checkbox_handlers() {
    const selectors = document.querySelectorAll('input[type=checkbox]');
    for (const el of selectors) {
        el.onclick = function(){checkbox_clicked(el)};
        div = el.nextSibling;
        div.onclick = function(){task_div_clicked(event, el.getAttribute('id'))};
        if (div.innerHTML.startsWith(' '))
            div.innerHTML = div.innerHTML.trim();
    }
}
var scroll_update_queued = false;
function on_scroll() {
    if (!scroll_update_queued) {
        scroll_update_queued = true;
        setTimeout(send_scroll_position, 250);
    }
}
function send_scroll_position() {
    window.webkit.messageHandlers.toPython.postMessage(
        {type: 'scrollPosition', position: get_scroll_position_proportion()});
    scroll_update_queued = false;
}
function get_scroll_position_proportion() {
    let el = document.documentElement;
    return el.scrollTop / (el.scrollHeight - el.clientHeight);
}
window.onload = function() {
    if (%s)
        add_checkbox_handlers();
    window.addEventListener('scroll', on_scroll);
    %s
}

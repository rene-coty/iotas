<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk" version="4.0"/>
  <template class="PreferencesDialog" parent="AdwPreferencesDialog">

    <child>
      <object class="AdwPreferencesPage">
        <property name="icon-name">settings-symbolic</property>
        <!-- Translators: Title -->
        <property name="title" translatable="yes">Interface</property>
        <child>
          <object class="AdwPreferencesGroup">
            <!-- Translators: Title -->
            <property name="title" translatable="yes">Editor</property>
            <child>
              <object class="AdwSwitchRow">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Use Monospace Font</property>
                <property name="action-name">settings.use-monospace-font</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Check Spelling</property>
                <property name="action-name">settings.spelling-enabled</property>
                <!-- Translators: Description, help -->
                <property name="subtitle" translatable="yes">Change language via the editor context menu</property>
              </object>
            </child>
            <child>
              <object class="AdwComboRow" id="_editor_header_bar_visibility_combo">
                <!-- Translators: Description, preference -->
                <property name="title" translatable="yes">Header Bar</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_editor_line_length">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Limit Line Length</property>
                <property name="action-name">settings.line-length</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Primarily for desktop. Use Ctrl + ↑ and Ctrl + ↓ on keyboard to fine tune.</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup">
            <!-- Translators: Title -->
            <property name="title" translatable="yes">Markdown</property>
            <child>
              <object class="AdwSwitchRow" id="_editor_detect_syntax">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Detect Syntax</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Required for syntax highlighting and formatting (toolbar and keyboard shortcuts). Disable for slightly improved performance.</property>
                <property name="action-name">settings.markdown-syntax-detection-enabled</property>
              </object>
            </child>
            <child>
              <object class="AdwComboRow" id="_editor_theme_combo">
                <!-- Translators: Description, preference -->
                <property name="title" translatable="yes">Theme</property>
              </object>
            </child>
            <child>
              <object class="AdwComboRow" id="_editor_formatting_bar_visibility_combo">
                <!-- Translators: Description, preference -->
                <property name="title" translatable="yes">Formatting Bar</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_enable_render_view">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Enable Formatted View</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Disable to reduce startup time</property>
                <property name="action-name">settings.markdown-render-enabled</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_markdown_default_to_render">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Open In Formatted View</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Enabling opens all notes as rendered markdown</property>
                <property name="action-name">settings.markdown-default-to-render</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_enable_markdown_maths">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Support Math Equations</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Slightly decreases render performance</property>
                <property name="action-name">settings.markdown-tex-support</property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_markdown_use_monospace_font">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Render Using Monospace Font</property>
                <property name="action-name">settings.markdown-use-monospace-font</property>
              </object>
            </child>
            <child>
              <object class="AdwSpinRow" id="_markdown_monospace_font_ratio">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Proportional To Monospace Font Size Ratio</property>
                <property name="subtitle" translatable="yes">In render view. Use 1 for no adjustment.</property>
                <property name="selectable">False</property>
                <property name="numeric">True</property>
                <property name="digits">2</property>
                <property name="adjustment">
                  <object class="GtkAdjustment">
                    <property name="lower">0.05</property>
                    <property name="upper">10</property>
                    <property name="step-increment">0.05</property>
                  </object>
                </property>
              </object>
            </child>
            <child>
              <object class="AdwSwitchRow" id="_keep_webkit_process">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Hold Engine In Memory</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Faster subsequent conversions for higher memory usage</property>
                <property name="action-name">settings.markdown-keep-webkit-process</property>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="AdwPreferencesGroup" id="_index_group">
            <!-- Translators: Title -->
            <property name="title" translatable="yes">Index</property>
            <child>
              <object class="AdwSwitchRow">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Pin Sidebar</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">On desktop, when there is space</property>
                <property name="selectable">False</property>
                <property name="action-name">settings.persist-sidebar</property>
              </object>
            </child>
            <child>
              <object class="AdwComboRow" id="_index_category_style_combo">
                <!-- Translators: Description, preference -->
                <property name="title" translatable="yes">Category Label Style</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="AdwPreferencesPage">
        <property name="icon-name">drive-harddisk-symbolic</property>
        <!-- Translators: Title -->
        <property name="title" translatable="yes">Data</property>
        <child>
          <object class="AdwPreferencesGroup">
            <child>
              <object class="AdwActionRow">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Reset Database</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Delete all notes from the local database. The app will quit.</property>
                <property name="selectable">False</property>
                <child>
                  <object class="GtkButton" id="_reset_database">
                    <property name="halign">start</property>
                    <property name="valign">center</property>
                    <!-- Translators: Button -->
                    <property name="label" translatable="yes">Reset</property>
                    <signal name="clicked" handler="_reset_database"/>
                    <style>
                      <class name="destructive-action"/>
                    </style>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="AdwActionRow" id="_disconnect_sync">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Disconnect Nextcloud</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Signs out from Nextcloud Notes. All notes will be removed and the app will quit.</property>
                <property name="selectable">False</property>
                <child>
                  <object class="GtkButton">
                    <property name="halign">start</property>
                    <property name="valign">center</property>
                    <!-- Translators: Button -->
                    <property name="label" translatable="yes">Disconnect</property>
                    <signal name="clicked" handler="_disconnect_nextcloud"/>
                    <style>
                      <class name="destructive-action"/>
                    </style>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>

    <child>
      <object class="AdwPreferencesPage" id="_debug_page">
        <property name="icon-name">bug-symbolic</property>
        <!-- Translators: Title -->
        <property name="title" translatable="yes">Debug</property>
        <child>
          <object class="AdwPreferencesGroup">
            <child>
              <object class="AdwActionRow">
                <!-- Translators: Title -->
                <property name="title" translatable="yes">Clear Sync Timestamp</property>
                <!-- Translators: Description, preference -->
                <property name="subtitle" translatable="yes">Forces a pull of all notes from the sync server</property>
                <property name="selectable">False</property>
                <child>
                  <object class="GtkButton">
                    <property name="halign">start</property>
                    <property name="valign">center</property>
                    <!-- Translators: Button -->
                    <property name="label" translatable="yes">Clear</property>
                    <signal name="clicked" handler="_reset_prune_threshold"/>
                    <style>
                      <class name="destructive-action"/>
                    </style>
                  </object>
                </child>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>

  </template>
</interface>

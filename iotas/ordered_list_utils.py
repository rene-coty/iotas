import re
from typing import Optional


def check_line_for_ordered_list_item(line: str) -> Optional[re.Match]:
    """Check whether string representing line of text contains an ordered list item.

    :param str line: Line of text to search
    :return: Regular expression match object, if matched
    :rtype: re.Match, optional
    """
    term = r"^(\s*)([a-zA-Z]{1}|[0-9]+)([\.\)]){1}[ ]+"
    return re.search(term, line)


def calculate_ordered_list_index(reference: str, direction: int) -> Optional[str]:
    """Calculate an item in an ordered list sequence.

    :param str reference: The element to start from
    :param int direction: The direction, 1 or -1
    :return: The item
    :rtype: str, optional
    """
    sequence_next = None
    if reference.isdigit():
        sequence_next = str(int(reference) + direction)
        if sequence_next == 0:
            sequence_next = None
    elif direction > 0 and reference.upper() != "Z":
        sequence_next = chr(ord(reference) + 1)
    elif direction < 0 and reference.upper() != "A":
        sequence_next = chr(ord(reference) - 1)
    return sequence_next


def format_ordered_list_item(spacing: str, index: str, delimiter: str) -> str:
    """Generate string for list item.

    :param str spacing: Pre marker spacing
    :param str index: Item index
    :param str delimiter: The delimiter before the item text
    :return: The built string
    :rtype: str
    """
    return f"{spacing}{index}{delimiter} "

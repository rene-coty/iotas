from abc import ABC, abstractmethod


class HtmlGeneratorConfiguration(ABC):
    """HTML generator configuration interface."""

    @abstractmethod
    def get_markdown_tex_support() -> bool:
        """Get whether markdown TeX rendering is supported.

        :return: Markdown TeX supported
        :rtype: bool
        """
        raise NotImplementedError()

    @abstractmethod
    def get_line_length() -> int:
        """Get line length.

        :return: Size in pixels
        :rtype: int
        """
        raise NotImplementedError()

    @abstractmethod
    def get_markdown_use_monospace_font() -> bool:
        """Get whether to use a monospace font for the markdown render.

        :return: Using monospace font
        :rtype: bool
        """
        raise NotImplementedError()

    @abstractmethod
    def get_use_monospace_font() -> bool:
        """Get whether to use a monospace font.

        :return: Using monospace font
        :rtype: bool
        """
        raise NotImplementedError()

    @abstractmethod
    def get_font_size() -> int:
        """Get font size.

        :return: Size
        :rtype: int
        """
        raise NotImplementedError()

    @abstractmethod
    def get_high_contrast() -> bool:
        """Get whether high contrast output should be used for screen rendering.

        :return: Whether visible
        :rtype: bool
        """
        raise NotImplementedError()

    @abstractmethod
    def get_markdown_render_monospace_font_ratio() -> float:
        """Get the adjustment in size from proportional to fixed width font.

        :return: Ratio
        :rtype: float
        """
        raise NotImplementedError()

    @abstractmethod
    def get_editor_header_bar_visible_for_window_state() -> bool:
        """Get whether the header bar is configured visible for the window maximised state."

        :return: Whether visible
        :rtype: bool
        """
        raise NotImplementedError()

    @abstractmethod
    def get_toolbar_underlay_padding_height() -> int:
        """Fetch the height of padding to account for overlay header bars.

        :return: Height in pixels
        :rtype: int
        """
        raise NotImplementedError()

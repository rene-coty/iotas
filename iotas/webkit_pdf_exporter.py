from gi.repository import GObject, GLib, Gtk

import logging
from typing import Callable

from iotas.note import Note
from iotas.pdf_exporter import PdfExporter


class WebKitPdfExporter(PdfExporter):

    __gsignals__ = {
        "finished": (GObject.SignalFlags.RUN_FIRST, None, ()),
        # Reason
        "failed": (GObject.SignalFlags.RUN_FIRST, None, (str,)),
    }

    # webkit type hinting avoided to allow for module lazy loading
    def __init__(self, webkit, keep_webkit_process: bool) -> None:
        super().__init__()
        self.__render_view = webkit
        self.__render_view.connect("loaded", lambda _o: self.__on_web_view_loaded())
        self.__keep_webkit_process = keep_webkit_process
        self.__finished_callback = None
        self.__error_callback = None
        self.__active = False
        self.__in_error = False

    def set_callbacks(self, finished_callback: Callable, error_callback: Callable) -> None:
        """Set functions to be called upon export result.

        :param Callable finished_callback: Finished callback
        :param Callable error_callback: Error callback
        """
        self.__finished_callback = finished_callback
        self.__error_callback = finished_callback

    def export(self, note: Note, location: str) -> None:
        """Export PDF of note.

        :param Note note: Note to export
        :param str location: Destination location
        """
        self.__note = note
        self.__location = location
        self.__active = True
        self.__in_error = False

        # Part of the effort to delay all WebKit initialisation
        if "WebKit" not in globals():
            import gi

            gi.require_version("WebKit", "6.0")
            global WebKit
            from gi.repository import WebKit

        self.__render_view.render_retaining_scroll(self.__note, "pdf")

    def __on_web_view_loaded(self) -> None:
        if not self.__active:
            return

        settings = Gtk.PrintSettings()
        settings.set(Gtk.PRINT_SETTINGS_PRINTER, "Print to File")
        settings.set(Gtk.PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf")
        settings.set(Gtk.PRINT_SETTINGS_OUTPUT_URI, f"file://{self.__location}")

        page_setup = Gtk.PageSetup()
        page_setup.set_left_margin(54, Gtk.Unit.POINTS)
        page_setup.set_right_margin(54, Gtk.Unit.POINTS)

        operation = WebKit.PrintOperation.new(self.__render_view)
        operation.set_print_settings(settings)
        operation.set_page_setup(page_setup)

        def finished(print_operation):
            logging.info(f"Exported pdf to {self.__location}")
            if not self.__keep_webkit_process:
                logging.info("Terminating WebKit process as holding disabled in preference")
                self.__render_view.terminate_web_process()
            if not self.__in_error:
                self.__finished_callback()
            self.__active = False

        def failed(print_operation, error: GLib.Error):
            logging.warning(f"Failed to export pdf to {self.__location}: %s", error.message)
            if not self.__keep_webkit_process:
                logging.info("Terminating WebKit process as holding disabled in preference")
                self.__render_view.terminate_web_process()
            self.__in_error = True
            self.__error_callback(error.message)

        operation.connect("finished", finished)
        operation.connect("failed", failed)
        operation.print_()
